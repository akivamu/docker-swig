# docker-swig

Linux docker image for [SWIG](swig.org) (Simplified Wrapper and Interface Generator)

`Dockerfile` taken from [this gist](https://gist.github.com/rcoup/35e11e31d069689c9c09a70cc2fc7a0d)

Manual build your image:

`docker build --build-arg SWIG_VERSION=3.0.10 -t swig .`

## Gitlab CI build

CI build swig version base on branch name
Only build on branches (none master), so name your branch match with swig version
